package com.flerry.audio.vk;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

public class Getaudio {
	public static int offset = 0;
	public static int counter;

	public static void allAudio() {
		int i = 0;
		Integer parsecounter1 = Integer.valueOf(Authorization.parsecount) - 1;
		String parsecount = String.valueOf(Authorization.parsecount);
		String query = "https://api.vk.com/method/audio.get?owner_id=" + Authorization.id + "&count=" + parsecount + "&"
				+ "access_token=" + Authorization.token + "&v=5.57";
		
		
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			connection.setRequestMethod("GET");

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				String response;
				while ((response = in.readLine()) != null) {
					JSONObject obj = new JSONObject(response);
					JSONArray parseall = obj.getJSONObject("response").getJSONArray("items");
					try {
						for (counter = 0; counter <= parsecounter1; counter++) {

							JSONObject all = parseall.getJSONObject(counter);
							String song_name = all.getString("title");
							String artist_name = all.getString("artist");

							String song_namefile = all.getString("title");
							String artist_namefile = all.getString("artist");
							URL audiourl = null;

							audiourl = new URL(all.getString("url"));
							System.out.println();
							System.out.println(song_name + " " + "|" + " " + artist_name + "\n\n");
							String filename = artist_namefile + " " + song_namefile + ".mp3";
							Download.load(audiourl, filename, 1);

						}

					} catch (org.json.JSONException e) {
						Textwork.notReceivedAuio();

					}
					System.out.println();
					System.out.println("�������� ������ �� ����������:" + " " + i + " " + "��" + " " + parsecount);
					System.out.println("������� �� ���-������ ���?" + "\n\n" + "[1] - �� (������� � ������ ������)"
							+ "\n\n" + "[2] - ����� �� ���������");
					Scanner resume = new Scanner(System.in);
					
					if (resume.hasNextInt()) {
						int b = resume.nextInt();
						if (b == 1) {
							Submain.start_display();
						}
						if (b == 2) {
							System.exit(0);
						}else{
							System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
							Submain.start_display();
						}

					}
				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());
			}
		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public static void randomAudio() {
		int i = 0;
		int random_offset = (int) (Math.random() * 299) + 2;
		String query = "https://api.vk.com/method/audio.getPopular?count=5&offset=" + random_offset + "&access_token="
				+ Authorization.token + "&v=5.57";

		Main.createfolder();
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;
				try {
					while ((response = in.readLine()) != null) {
						JSONObject obj = new JSONObject(response);
						JSONArray parseall = obj.getJSONArray("response");
						JSONObject all = parseall.getJSONObject(i);
						String song_name = all.getString("title");
						String artist_name = all.getString("artist");

						String song_namefile = all.getString("title");
						String artist_namefile = all.getString("artist");
						URL audiourl = null;

						audiourl = new URL(all.getString("url"));
						System.out.println();
						System.out.println(song_name + " " + "|" + " " + artist_name + "\n\n");
						String filename = artist_namefile + " " + song_namefile + ".mp3";
						System.out.println(
								"�������?" + "\n\n" + "[1] - ��" + "\n\n" + "[2] - ��� (����� ������ ��������)" + "\n\n"
										+ "[3] - ������� � ������ ������" + "\n\n" + "[4] - ����� �� ���������");
						Scanner good_or_no = new Scanner(System.in);
						if (good_or_no.hasNextInt()) {
							int good = good_or_no.nextInt();
							if (good == 1) {
								Download.load(audiourl, filename, 0);
								System.out.println();
								System.out.println("����� ��� ���� ��������� �����?" + "\n\n" + "[1] - ��" + "\n\n"
										+ "[2] - ��� (������� � ������ ������)" + "\n\n" + "[3] - ����� �� ���������");
								
								Scanner resume = new Scanner(System.in);
								if (resume.hasNextInt()) {
									int b = resume.nextInt();
									if (b == 1) {
										Getaudio.randomAudio();
									}
									if (b == 2) {
										Submain.start_display();
									}
									if (b == 3) {
										System.exit(0);
									}else{
										System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
										Submain.start_display();
									}
								}

							}
							if (good == 2) {
								System.out.println("��������� ��� ���-������..." + "\n\n");
								random_offset += 1;
								Getaudio.randomAudio();

							}
							if (good == 3) {
								Submain.start_display();
							}
							if (good == 4) {
								System.exit(0);
							}else{
								System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
							Submain.start_display();
							}
						}
					}

				} catch (org.json.JSONException e) {
					Textwork.notReceivedAuio();
					Getaudio.randomAudio();
				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());

			}

		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public static void smartAudio() {
		String query;
		query = "https://api.vk.com/method/audio.getRecommendations?count=5&user_id=" + Authorization.id
				+ "&access_token=" + Authorization.token + "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;
				try {
					while ((response = in.readLine()) != null) {
						JSONObject ob = new JSONObject(response);
						JSONArray parseall = ob.getJSONObject("response").getJSONArray("items");
						try {

							for (int i = 0; i <= 10; i++) {

								JSONObject all = parseall.getJSONObject(i);
								String song_name = all.getString("title");
								String artist_name = all.getString("artist");
								URL audiourl = null;

								audiourl = new URL(all.getString("url"));
								System.out.println();
								System.out.println(song_name + " " + "|" + " " + artist_name + "\n\n");
								System.out.println("[Download URL]" + audiourl);

							}

						} catch (org.json.JSONException e) {

							Getaudio.smartAudio();

						}
					}
					System.out.println("\n\n������������ ����� ������������?\n\n" + "[1] - ��" + "\n\n"
							+ "[2] - ��� (������� � ������ ������)" + "\n\n" + "[3] - ����� �� ���������");
					Scanner uo = new Scanner(System.in);

					if (uo.hasNextInt()) {
						int b = uo.nextInt();
						if (b == 1) {
							Getaudio.smartAudio();
							;
						}
						if (b == 2) {
							Submain.start_display();
						}
						if (b == 3) {
							System.exit(0);
						}else{
							System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
							Submain.start_display();
							}
					}
				} catch (org.json.JSONException e) {

				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());

			}

		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public static void searchPerformer() throws UnsupportedEncodingException {
		int random_offset = (int) (Math.random() * 250) + 1;
		String searchname = null;
		String messenge = "������� ���������� ����������?\n\n";
		System.out.println("������� ������ (���������, Beatles)...");
		Scanner sc1 = new Scanner(System.in);
		if (sc1.hasNextLine()) {
			searchname = sc1.nextLine();
		}
		String query;
		query = "https://api.vk.com/method/audio.search?&q=" + URLEncoder.encode(searchname, "UTF-8")
				+ "&auto_complete=1&performer_only=1&offset=" + random_offset + "&count=10&access_token="
				+ Authorization.token + "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;
				try {
					while ((response = in.readLine()) != null) {
						Parseresponse.parse(response, messenge);
						System.out.println("[1] - ��" + "\n\n" + "[2] - ��� (����� ������ ��������)" + "\n\n"
								+ "[3] - ������� � ������ ������" + "\n\n" + "[4] - ����� �� ���������");
						Scanner good_or_no = new Scanner(System.in);
						if (good_or_no.hasNextInt()) {
							int good = good_or_no.nextInt();
							if (good == 1) {
								Download.load(Parseresponse.audiourl, Parseresponse.filename, 0);
								System.out.println();
								System.out.println("������� �� ���-������ ���?" + "\n\n" + "[1] - ��" + "\n\n"
										+ "[2] - ��� (������� � ������ ������)" + "\n\n" + "[3] - ����� �� ���������");
								Scanner resume = new Scanner(System.in);
								if (resume.hasNextInt()) {
									int b = resume.nextInt();
									if (b == 1) {
										Getaudio.searchPerformer();
										;
									}
									if (b == 2) {
										Submain.start_display();
									}
									if (b == 3) {
										System.exit(0);
									}else{
										System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
										Submain.start_display();
									}
								}

							}
							if (good == 2) {
								System.out.println("��������� ����� �����" + "\n\n"
										+ "[��������] ��� ����� ���� �� �������� ����� ���������� ������ ����� �� ����� �������� (������������� �� ������ ������ ��)"
										+ "\n\n");
								Getaudio.offset += 1;
								Getaudio.searchPerformer();
								;

							}
							if (good == 3) {
								Submain.start_display();
							}
							if (good == 4) {
								System.exit(0);
							}else{
								System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
								Submain.start_display();
							}

						}
					}

				} catch (org.json.JSONException e) {

					Getaudio.searchPerformer();
					;
				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());

			}

		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public static void searchOwn() throws UnsupportedEncodingException {
		String searchname = null;
		System.out.println("������� �������� ����������...");
		Scanner sc1 = new Scanner(System.in);
		if (sc1.hasNextLine()) {
			searchname = sc1.nextLine();
		}

		String query = "https://api.vk.com/method/audio.search?&q=" + URLEncoder.encode(searchname, "UTF-8")
				+ "&auto_complete=1&lyrics=0&search_own=1&count=1&access_token=" + Authorization.token + "&v=5.57";
		String messenge = null;
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;
				try {
					while ((response = in.readLine()) != null) {
						Parseresponse.parse(response, messenge);

						Download.load(Parseresponse.audiourl, Parseresponse.filename, 0);
						System.out.println();
						System.out.println("������� �� ���-������ ���?" + "\n\n" + "[1] - ��" + "\n\n"
								+ "[2] - ��� (������� � ������ ������)" + "\n\n" + "[3] - ����� �� ���������");
						Scanner resume = new Scanner(System.in);
						if (resume.hasNextInt()) {
							int b = resume.nextInt();
							if (b == 1) {
								Getaudio.searchOwn();
							}
							if (b == 2) {
								Submain.start_display();
							}
							if (b == 3) {
								System.exit(0);
							}else{
								System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
								Submain.start_display();
							}
						}

					}
				} catch (org.json.JSONException e) {
					Textwork.notReceivedAuio();
					Getaudio.searchOwn();

				}
			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());
			}
		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static void searchVk() throws UnsupportedEncodingException {
		int i = 0;
		String lyrics = null;

		System.out.println("������� �������� ����������...");
		Scanner sc1 = new Scanner(System.in);

		String searchname = sc1.nextLine();
		String messenge = "������� ���������� ����������?";
		String query;
		query = "https://api.vk.com/method/audio.search?&q=" + URLEncoder.encode(searchname, "UTF-8")
				+ "&auto_complete=1&offset=" + offset + "&lyrics=" + lyrics + "&count=1&sort=2&access_token="
				+ Authorization.token + "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {

				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;
				try {
					while ((response = in.readLine()) != null) {

						for (i = 0; i < 1; i++) {

							Parseresponse.parse(response, messenge);
							System.out.println("[1] - ��" + "\n\n" + "[2] - ��� (����� ������ ��������)" + "\n\n"
									+ "[3] - ������� � ������ ������" + "\n\n" + "[4] - ����� �� ���������");

							Scanner good_or_no = new Scanner(System.in);
							if (good_or_no.hasNextInt()) {
								int good = good_or_no.nextInt();
								if (good == 1) {
									Download.load(Parseresponse.audiourl, Parseresponse.filename, 0);
									System.out.println();
									System.out.println("������� �� ���-������ ���?" + "\n\n" + "[1] - ��" + "\n\n"
											+ "[2] - ��� (������� � ������ ������)" + "\n\n"
											+ "[3] - ����� �� ���������");
									Scanner resume = new Scanner(System.in);
									if (resume.hasNextInt()) {
										int b = resume.nextInt();
										if (b == 1) {
											Getaudio.searchVk();
										}
										if (b == 2) {
											Submain.start_display();
										}
										if (b == 3) {
											System.exit(0);
										}else{
											System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
											Submain.start_display();
										}
									}

								}
								if (good == 2) {
									System.out.println("��������� ����� �����" + "\n\n"
											+ "[��������] ��� ����� ���� �� �������� ����� ���������� ������ ����� �� ����� �������� (������������� �� ������ ������ ��)"
											+ "\n\n");
									Getaudio.offset += 1;
									Getaudio.searchVk();

								}
								if (good == 3) {
									Submain.start_display();
								}
								if (good == 4) {
									System.exit(0);
								}else{
									System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
									Submain.start_display();
								}

							}
						}
					}

				} catch (org.json.JSONException e) {
					Textwork.notReceivedAuio();
					Getaudio.searchVk();
				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());

			}

		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public static int groupcounter;

	public static void getGroupAudio() {
		Integer parsecounter1 = Integer.valueOf(Authorization.groupcount) - 1;
		String parsecount = String.valueOf(Authorization.groupcount);
		System.out
				.println("������� ���������� �������, ������� ������ ������� (0 - ��� ������, max ���������� - 5000)");
		Scanner sc = new Scanner(System.in);
		String count = sc.nextLine();
		String query = "https://api.vk.com/method/audio.get?owner_id=-" + Authorization.group_id + "&count=" + count
				+ "&access_token=" + Authorization.token + "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			connection.setRequestMethod("GET");

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				String response;
				while ((response = in.readLine()) != null) {
					JSONObject obj = new JSONObject(response);
					JSONArray parseall = obj.getJSONObject("response").getJSONArray("items");
					try {
						for (groupcounter = 0; groupcounter <= parsecounter1; groupcounter++) {

							JSONObject all = parseall.getJSONObject(groupcounter);
							String song_name = all.getString("title");
							String artist_name = all.getString("artist");

							String song_namefile = all.getString("title");
							String artist_namefile = all.getString("artist");
							URL audiourl = null;

							audiourl = new URL(all.getString("url"));
							System.out.println();
							System.out.println(song_name + " " + "|" + " " + artist_name + "\n\n");
							String filename = artist_namefile + " " + song_namefile + ".mp3";
							Download.load(audiourl, filename, 2);

						}

					} catch (org.json.JSONException e) {
						System.out.println();
						System.out.println("[��������] �� �������� ��������� ������� ������������ [��������]");

					}
					System.out.println();
					System.out.println(
							"�������� ������ �� ����������:" + " " + groupcounter + " " + "��" + " " + parsecount);
					System.out.println("������� �� ���-������ ���?" + "\n\n" + "[1] - �� (������� � ������ ������)"
							+ "\n\n" + "[2] - ����� �� ���������");
					Scanner resume = new Scanner(System.in);
					if (resume.hasNextInt()) {
						int b = resume.nextInt();
						if (b == 1) {
							Submain.start_display();
						}
						if (b == 2) {
							System.exit(0);
						}else{
							System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
							Submain.start_display();
						}

					}
				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());
			}
		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	private static int lyrics_id;

	public static void getLyricsid() throws UnsupportedEncodingException {

		int i = 0;

		System.out.println("������� �������� ����������...");
		Scanner sc1 = new Scanner(System.in);

		String searchname = sc1.nextLine();

		String query;
		query = "https://api.vk.com/method/audio.search?&q=" + URLEncoder.encode(searchname, "UTF-8")
				+ "&auto_complete=1&lyrics=1&count=1&sort=2&access_token=" + Authorization.token + "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {

				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;
				try {
					while ((response = in.readLine()) != null) {

						for (i = 0; i < 1; i++) {

							JSONObject obj = new JSONObject(response);

							JSONArray parseall = obj.getJSONObject("response").getJSONArray("items");
							JSONObject all = parseall.getJSONObject(i);
							lyrics_id = all.getInt("lyrics_id");
							String song_name = all.getString("title");
							String artist_name = all.getString("artist");

							System.out.println();
							System.out.println("[�������]" + " " + song_name + " " + "|" + " " + artist_name + "\n\n");

						}

						System.out.println("������� ����� ��� ���� ����������?" + "\n\n" + "[1] - ��" + "\n\n"
								+ "[2] - ������� � ������ ������ " + "\n\n" + "[3] - ����� �� ���������" + "\n\n");
						Scanner good_or_no = new Scanner(System.in);
						if (good_or_no.hasNextInt()) {
							int good = good_or_no.nextInt();
							if (good == 1) {
								Getaudio.getLyrics();
							}

							if (good == 2) {
								Submain.start_display();

							}
							if (good == 3) {
								System.exit(0);
							}else{
								System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
								Submain.start_display();
							}

						}
					}

				} catch (org.json.JSONException e) {
					Textwork.notReceivedAuio();
					Getaudio.getLyricsid();
				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());

			}

		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static void getLyrics() throws UnsupportedEncodingException {
		System.out.println("������� �������� ����������, ��� ������� ����� �����...");
		Scanner sc1 = new Scanner(System.in);
		String query = "https://api.vk.com/method/audio.getLyrics?lyrics_id=" + lyrics_id + "&access_token="
				+ Authorization.token + "&v=5.57 ";
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;

				while ((response = in.readLine()) != null) {
					JSONObject obj = new JSONObject(response);
					JSONObject parseall = obj.getJSONObject("response");
					// JSONObject all = parseall.getJSONObject(i);
					String text = parseall.getString("text");
					System.out.println(text + "\n\n");

					System.out.println("����� ����� ��� ������ �����?" + "\n\n" + "[1] - ��" + "\n\n"
							+ "[2] - ��� (������� � ������ ������)" + "\n\n" + "[3] - ����� �� ���������");
					Scanner resume = new Scanner(System.in);
					if (resume.hasNextInt()) {
						int b = resume.nextInt();
						if (b == 1) {
							Getaudio.getLyricsid();
							;
						}
						if (b == 2) {
							Submain.start_display();
						}
						if (b == 3) {
							resume.close();
							System.exit(0);
						}else{
							System.out.println("�� ������� �������������� �������... ������� � ������ ������...");
							Submain.start_display();
						}
					}

				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());
			}
		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}
}
