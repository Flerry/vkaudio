
package com.flerry.audio.vk;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Authorization {

	public static String token;
	public static String id;

	public static void oauth() {
		System.out.println("����������, ���������� ������ ������ � �������:" + "\n\n"
				+ "https://oauth.vk.com/authorize?client_id=5642850&display=page&redirect_uri=https://oauth.vk.com/blank.html &scope=audio,offline&response_type=token&v=5.57&state=�����"
				+ "\n\n" + "� ����� � �������� ����� ������� ��� �����, �� ������� ���!" + "\n\n" + "����� �� ������ �� �������� ����� �����, � ������������ ������" + "\n\n");
		token = Gettoken_id.get_token();
		id = Gettoken_id.get_id();

	}

	public static String parsecount;

	public static void getcount() {
		String query = "https://api.vk.com/method/audio.getCount?owner_id=" + id + "&access_token=" + token + "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			connection.setRequestMethod("GET");

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;

				while ((response = in.readLine()) != null) {
					parsecount = response.substring(12, response.indexOf("}"));

					System.out.println("���������� ������������ �� ����� ��������:" + " " + parsecount);

				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());
			}
		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public static String groupcount;

	public static String group_id;

	public static void getCountGroup() {
		System.out.println("������� id ������...");
		Scanner sc = new Scanner(System.in);
		if (sc.hasNextLine()) {
			group_id = sc.nextLine();
		}
		String query = "https://api.vk.com/method/audio.getCount?owner_id=-" + group_id + "&access_token=" + token
				+ "&v=5.57";

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(query).openConnection();

			connection.setRequestMethod("GET");

			if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String response;

				while ((response = in.readLine()) != null) {
					groupcount = response.substring(12, response.indexOf("}"));

					System.out.println("���������� ������������ �� �������� ����������:" + " " + groupcount);

				}

			} else {
				System.out.println("fail:" + connection.getResponseCode() + ", " + connection.getResponseMessage());
			}
		} catch (Throwable cause) {
			cause.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

}
