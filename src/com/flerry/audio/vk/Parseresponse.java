package com.flerry.audio.vk;

import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Parseresponse {
	public static String song_name;
	public static String artist_name;
	public static String song_namefile;
	public static String artist_namefile;
	public static String filename;
	public static String messenge;
	public static URL audiourl;

	public static void parse(String response, String messenge) {
		int i = 0;

		JSONObject obj = new JSONObject(response);
		JSONArray parseall = obj.getJSONObject("response").getJSONArray("items");
		JSONObject all = parseall.getJSONObject(i);
		song_name = all.getString("title");
		artist_name = all.getString("artist");

		song_namefile = all.getString("title");
		artist_namefile = all.getString("artist");
		audiourl = null;

		try {
			audiourl = new URL(all.getString("url"));
		} catch (MalformedURLException e) {
			System.out.println("");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println();
		System.out.println(song_name + " " + "|" + " " + artist_name + "\n\n");
		filename = artist_namefile + " " + song_namefile + ".mp3";
		if (messenge != null) {
			System.out.println(messenge);
		}
	}

}
